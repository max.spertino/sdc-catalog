import axios from "axios";

export const getServices = async (tenant) => {
    try {
        const response = await axios(`https://sdctest.boat.opencontent.io/${tenant}/api/services`);
        if(response.data.length > 0) {
            return response.data.reduce((filtered, record) => {
                if (record.status === 1 && record.flow_steps.length > 0) {
                    filtered.push(record);
                }
                return filtered;
            }, [])
        }
    } catch (e) {
        console.log(e);
        return [];
    }
};


export const getAllServices = async () => {
    try {
        const response = await axios('http://0.0.0.0:3001/api/services');
        const data = response.data;
        return JSON.parse(data);
    } catch (e) {
        console.log(e);
        return [];
    }
};

/*
export const getAllServices = async () => {
    try {
        const response = await axios('https://sdctest.boat.opencontent.io/prometheus.json');
        const responseAll = [];
        for(const record of response.data) {
            const tenant = record.labels.__metrics_path__.replace(/\/|metrics/g,"");
            const services = await getServices(tenant);
            for(const prop in services) {
                responseAll[services[prop].slug] = services[prop];
            }
        }
        return Object.values(responseAll);
    } catch (e) {
        console.log(e);
        return [];
    }
};
*/