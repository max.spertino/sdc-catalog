import React, {useEffect, useState} from 'react'
import { useParams } from "react-router-dom";
import {
  Breadcrumb,
  BreadcrumbItem,
  Card,
  CardBody,
  CardTitle,
  CardText,
  CardReadMore,
  Container,
  Icon,
  Input,
  Row,
  Col,
  LinkListItem,
  LinkList,
  UncontrolledCollapse
} from 'design-react-kit'

import CompleteHeader from './shared/Header'
import Footer from './shared/Footer'
import { getServices, getAllServices } from '../utils/getData';

const Services = () => {
 
  const { ente } = useParams();

  const [data, setData] = useState({services: [], isFetching: false});
  useEffect(() => {

    const getAllData = async () => {
      setData({services: [], isFetching: true});
      let services = [];
      if(ente) {
        services = await getServices(ente);
      } else {
        services = await getAllServices();
      }
      setData({services, isFetching: false});
    };
    getAllData();

  }, [ente]);
  // TODO find a better way to handle this
  // Storyshot does not use the dom so can't render refs
  // to fix the problem we append the elements manually
  // this fixes tests without touching the rendered components
  // nor storybook
  // https://github.com/storybookjs/storybook/issues/886
  // https://github.com/infinitered/addon-storyshots#using-createnodemock-to-mock-refs
  const div = document.createElement('div')
  div.setAttribute('id', 'altri-servizi')
  document.body.appendChild(div)


  const townName = ente;
  const townTagLine = 'Uno dei tanti Comuni d Italia';

  return (
    <>
      <CompleteHeader
        sticky
        theme=""
        townName={townName}
        townTagLine={townTagLine}
      />
      <main>
        <Container tag="section" id="briciole" className="px-4 my-4">
          <Row>
            <Col className="px-lg-4">
              <nav aria-label="breadcrumb" className="breadcrumb-container">
                <Breadcrumb>
                  <BreadcrumbItem>
                    <a href="/design-comuni-prototipi/esempi/bootstrap-italia/template-homepage.html">
                      Home
                    </a>
                    <span className="separator">/</span>
                  </BreadcrumbItem>
                  <BreadcrumbItem aria-current="page" active>
                    Servizi
                  </BreadcrumbItem>
                </Breadcrumb>
              </nav>
            </Col>
          </Row>
        </Container>
        <Container tag="section" id="intro" className="px-4 my-4">
          <Row>
            <Col lg={7} className="px-lg-4 py-lg-2">
              <h1>Servizi</h1>
              <p>
                Pagamenti, domande e iscrizioni, contributi e graduatorie, segnalazioni, autorizzazioni e concessioni,
                certificati e dichiarazioni servizi pubblici.
              </p>
              <div className="form-group mt-5">
                <form>
                  <Input
                    id="ricerca-servizi"
                    type="search"
                    label='Cerca contenuti in "Servizi"'
                  />
                  <span aria-hidden="true" className="autocomplete-icon">
                    <Icon icon="it-search" size="sm" />
                  </span>
                </form>
              </div>
              <div id="filtri-ricerca-amministrazione">
                <h6 className="small">Filtri</h6>
                <div className="chip chip-lg">
                  <span className="chip-label">Tutto</span>
                  <button>
                    <Icon icon="it-close" />
                    <span className="sr-only">Elimina label</span>
                  </button>
                </div>
                <div className="ml-2 d-inline">
                  <button className="btn btn-icon btn-outline-primary btn-sm align-top">
                    <Icon icon="it-plus" color="primary" />
                    <span>Aggiungi filtro</span>
                  </button>
                </div>
              </div>
            </Col>
            <Col lg={{ size: 4, offset: 1 }} className="pt-5 pt-lg-2">
              <LinkList className="footer-list clearfix">
                <LinkListItem key={1} tag="h3" header>
                  Tutti i Servizi
                </LinkListItem>
                {!data.isFetching && data.services.slice(0, 5).map(label => 
                    <LinkListItem key={label.id} href="#">
                      <span>{label.name}</span>
                    </LinkListItem>
                  )
                }
                <LinkListItem
                  className="large medium left-icon"
                  aria-expanded="true"
                  aria-controls="altri-servizi"
                  id="altri-servizi">
                  <Icon
                    icon="it-more-items"
                    color="primary"
                    className="right"
                  />
                </LinkListItem>
                <UncontrolledCollapse
                  toggler="#altri-servizi"
                  className="link-sublist px-0"
                  tag="ul">
                  {!data.isFetching && data.services.slice(6).map(label => 
                      <LinkListItem key={label.id} href="#">
                        <span>{label.name}</span>
                      </LinkListItem>
                    )
                  }
                </UncontrolledCollapse>
                <LinkListItem className="medium" href="#">
                  <span>Tutti i servizi</span>
                </LinkListItem>
              </LinkList>
            </Col>
          </Row>
        </Container>
        <section id="in-evidenza">
          <div className="bg-light py-5">
            <Container className="px-4">
              <Row>
                <Col>
                  <h3 className="mb-4">Tutti i Servizi</h3>
                </Col>
              </Row>
              <Row>
                {!data.isFetching && data.services.map(label => 
                    <Col size="12" sm={6} lg={4} key={label.id}>
                      <article className="card-wrapper card-space">
                        <Card
                          noWrapper
                          className="card-bg card-big rounded shadow">
                          <CardBody>
                            <CardTitle tag="h5">
                            <Icon icon='it-file' />{label.name}
                            </CardTitle>
                            <CardText>
                              {`${label.description.replace(/(<([^>]+)>)/gi, "").substring(0, 100)}...`}
                            </CardText>
                            <CardReadMore
                              icon="it-arrow-right"
                              text="Leggi di più"
                              href="#"
                            />
                          </CardBody>
                        </Card>
                      </article>
                    </Col>
                  )
                }
              </Row>
            </Container>
          </div>
        </section>
      </main>
      <Footer townName={townName} townTagLine={townTagLine} />
    </>
  )
}

export default Services