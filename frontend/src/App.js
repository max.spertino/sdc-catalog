import React from 'react';
import './App.css';
import 'bootstrap-italia/dist/css/bootstrap-italia.min.css';
import 'typeface-titillium-web';
import 'typeface-roboto-mono';
import 'typeface-lora';
import Services from './components/Services';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

const App = () => {
    return (
      <Router>
        
          <Switch>
            <Route path="/:ente/servizi" children={<Services />} />
            <Route path="/servizi" children={<Services />} />
          </Switch>
      </Router>
    );
};

export default App;