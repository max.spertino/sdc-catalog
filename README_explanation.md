# Sdc catalog Monorepo
Ho realizzato un ambiente di sviluppo e di testing dockerizzato, pensato per un rilascio in singoli container instances, web apps o da integrare in pods su un cluster. L'ambiente è formato da 2 applicativi: frontend e backend.

La criticità maggiore è stata capire a come reperire i dati in maniera efficiente. Essendo disaggregati richiedono chiamate multiple alle api di stanza del cittadino. Ho quindi pensato ad un servizio node che aggregasse i dati e li esponesse a Frontend. Per semplicità adesso i dati sono salvati in un file json ma su questo punto ci sono diverse alternative, tra cui appoggiarsi ad un database o ad un sistema di caching (vedi Redis).

Il Backend adesso ha 2 funzioni: di aggregazione dei dati ed esposizione delle api rest. Si può pensare di suddividere per funzione e dominio il Backend in 2 servizi distinti: il primo che aggrega i dati (con un sistema di scheduling) e li salva in un database (vista la disomogeneità dei dati un Mongo db offre maggiore flessibilità) ed il secondo che offre dei servizi rest efficienti sui dati aggreagati. Questi ragionamenti portano naturalmente ad organizzare il progetto con un'architettura a microservizi (per scalabilità e flessibilità), dove abbiamo 3 servizi: Frontend, Aggregatore con database, Api.


## Frontend
Presenta l'elenco trasversale dei servizi.
E' scritto in React. Utilizza i componenti design kit react che implementano la libreria Boostrap Italia.
Fa una singola chiamata ad un servizio rest (Backend) che espone i dati aggregati.

## Backend
E' un server node.

Implementa uno scheduler che esegue un job. Il job richiama un servizio interno che aggrega i dati ottenuti da chiamate alle api della stanza del cittadino. I dati aggregati vengono salvati in un file json.

I dati contenuti nel file json sono esposti attraverso un servizio esterno che richiama il Frontend.

## Avviare il progetto localmente

Per installare tutte le dipendenze:

```shell script
npm run docker-compose:start
```

Per lanciare localmente l'ambiente di sviluppo

```shell script
npm run docker-compose:up
```

Per costruire l'immmagine docker da rilasciare in produzione:

```shell script
npm run docker-compose:build
```

## Server locali

Pagina di presentazione dei risultati

```url
http://127.0.0.1/servizi
```

Api rest che restituisce i dati aggregati

```url
http://0.0.0.0:3001/api/services
```

Presentazione dei servizi per comune

```url
http://127.0.0.1/comune-di-bugliano/servizi
```


## Principali librerie

- _Backend_:
  - **_node_**: <https://nodejs.org>
  - **_express_**: <https://github.com/expressjs/express>
  - **_cron_**: <https://github.com/kelektiv/node-cron>
- _Frontend_:
  - **_react_**: <https://reactjs.org/>
  - **_bootstrap-italia_**: <https://github.com/italia/bootstrap-italia>
  - **_design-react-kit_**: <https://github.com/italia/design-react-kit>
- _Utils_:
  - **_axios_**: <https://github.com/axios/axios>