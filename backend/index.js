const express = require('express'); 
const aggregators = require('./utils/aggregators');

const app = express();  
const router = express.Router();

const CronJob = require('cron').CronJob;

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://127.0.0.1');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type')
    next();
});

app.use('/api', router);

const PORT = process.env.CONTAINER_PORT || 3000;
const HOST = process.env.HOST || "0.0.0.0";
const HOST_PORT = process.env.HOST_PORT || 3000; 

router.get('/', function(req, res) {
    res.json({ message: 'Sdc catalog aggregation server: welcome to our api!' });   
});


router.get('/services', async (req, res) => {
    const result = await aggregators.getAllServicesJson();
    res.json(result.toString('utf8'));   
});

app.listen(PORT, HOST, () => {
    console.log(`Server running at http://${HOST}:${HOST_PORT}/`);
});


const job = new CronJob('0 0 */12 * * *', async () => {
    const d = new Date();
	console.log('Every 12 hours:', d);
    await aggregators.writeServicesToJson();
});
job.start();