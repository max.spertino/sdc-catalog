const axios = require('axios');
const fs = require('fs');

const getServices = async (tenant) => {
    try {
        const response = await axios(`https://www2.stanzadelcittadino.it/${tenant}/api/services`);
        if(response.data.length > 0) {
            return response.data.reduce((filtered, record) => {
                if (record.status === 1 && record.flow_steps.length > 0) {
                    filtered.push(record);
                }
                return filtered;
            }, [])
        }
    } catch (e) {
        console.log(e);
        return [];
    }
};

const getAllServices = async () => {
    try {
        const response = await axios('https://www2.stanzadelcittadino.it/prometheus.json');
        const responseAll = [];
        for(const record of response.data) {
            const tenant = record.labels.__metrics_path__.replace(/\/|metrics/g,"");
            const services = await getServices(tenant);
            for(const prop in services) {
                responseAll[services[prop].slug] = services[prop];
            }
        }
        return Object.values(responseAll);
    } catch (e) {
        console.log(e);
        return [];
    }
};


const writeServicesToJson = async () => {
    try {
        const data = await getAllServices();
        fs.promises.writeFile('aggregates/services.json', JSON.stringify(data));
        return data;
    } catch (e) {
        console.log(e);
        return [];
    }
};



const getAllServicesJson = async () => {
    try {
        const data = await fs.promises.readFile('aggregates/services.json');
        return data;
    } catch (e) {
        console.log(e);
        return [];
    }
};

exports.getAllServicesJson = getAllServicesJson
exports.writeServicesToJson = writeServicesToJson