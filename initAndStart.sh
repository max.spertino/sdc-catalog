#!/usr/bin/env bash

NODE_BASED_SERVICES=(
    frontend
    backend
)

if [[ $1 == "--install" || $1 == "--build" ]]; then
    echo "Updating for Monorepo"
    npm ci
    for SERVICE in "${NODE_BASED_SERVICES[@]}"; do
        echo "Updating for ${SERVICE}"
        (cd ./"${SERVICE}"/ && npm ci)
    done
    exit
fi

export CURRENT_UID=$(id -u)

if [[ $1 == "--build" ]]; then
    docker-compose -f docker-compose.prod.yml build --parallel --pull
else
    docker-compose up
fi
